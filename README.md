###Twitter Bot Accidental Haiku´s###

A Python bot that filters existing tweets with 17 syllables, and retweets them formatted as haiku´s (A Japanese form of poems).

### How to tweet from a Python Program###
To make a program that can tweet, follow these steps:
1.Install the Tweepy library --> ( $ pip3 install --user tweepy).
2.Create a Twitter account for your program/app/bot.
3.Download the access credentials.
4.Get started with Tweepy.

### Create Twitter account ###
First make a Twitter account on https://twitter.com.
After that, you will need to create an app account on https://dev.twitter.com/apps.
-Sign in with your Twitter account.
-Create a new app account.
-Modify the settings for that app account to allow read & write.
-Generate a new OAuth token with those permissions.

Following these steps will create four tokens that you will need to place in the right file discussed below.

### Configuring the bot###
To run the bot properly, you must first set it up so it can connect to the Twitter API. Fill in the following information with the 4 tokens you got from your Twitter app account in the twitterbot.py file:
-CONSUMER_KEY
-CONSUMER_SECRET 
-ACCESS_KEY 
-ACCESS_SECRET 

### How to make the Twitter bot work ###
To start the program, type in the following command in your terminal:
$ python3 GUI.py  

Now you get a window with the name of the Twitter bot (Haiku Banaan) and two buttons:
1. Generate Haiku --> One click on this button generates a new tweet.
2. Tweet Haiku --> One click on this button pushes the tweet to the World Wide Web.

###Authors###
Stan van Suijlekom,
Wadi Naji,
Ruben Oosterloo