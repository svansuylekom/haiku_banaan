#!/usr/bin/python3

import csv
from twitterbot import tweet
import os
import os.path

# The file from which you read tweets
FILENAME = "08.txt"

def get_syllable_dict():
    ''' Reads dictionary from the literals.csv-file and returns it '''
    syllables = {}
    
    for key, val in csv.reader(open("literals.csv", "r")):
        syllables[key] = val
    return syllables

def strip_tweet(line):
    ''' Remove username, hashtags and tagged persons from tweet '''
    words = line.lower().split()
    # Remove username from tweet
    words = words[1:]
    counter = 0
    
    for word in words:
        if word[0] in '@#':
            # Remove hashtags and tagged persons from tweet
            words.pop(counter)
            counter -= 1
        else:
            counter += 1
    return words  
    
def valid_tweet(word_list, syllables): 
    ''' Check if words in the tweet are in 'syllables' and if the tweet contains 17 syllables '''
    nr_syllables = 0  
    
    for word in word_list:
        if word not in syllables:
            return False
        else:
            nr_syllables += int(syllables[word])
    if nr_syllables == 17:
        return True
    else:
        return False

def make_haiku(word_list, syllables):
    ''' Makes haiku using the words in word_list '''
    nr_syllables = 0
    second_line = third_line = 0
    haiku = ""
    
    for word in word_list:
        nr_syllables += int(syllables[word])
        haiku = haiku + word + ' '
        if nr_syllables == 5 and second_line == 0:
            haiku = haiku + '/'
            second_line = 1
        elif nr_syllables > 5 and second_line == 0:
            return False
        elif nr_syllables == 12 and second_line == 1:
            haiku = haiku + '/'
            third_line = 1
        elif nr_syllables > 12 and second_line == 1 and third_line == 0:
            return False
        elif nr_syllables == 17 and third_line == 1:
            haiku = haiku + '/' 
    
    return haiku  

def read_haikus(filename):
    ''' For every tweet: strip the tweet, check wether it's possible to make a haiku
    In case it is, make a haiku and write it to haiku.txt '''
    syllables = get_syllable_dict()
    filehandle = open(filename, "r")
    result = open("haiku.txt", "w")
    
    for line in filehandle:
        word_list = strip_tweet(line)
        if valid_tweet(word_list, syllables):
            haiku = make_haiku(word_list, syllables)
            if haiku != False:
                print(haiku, file=result)
                
    filehandle.close()
    result.close()    

def format_haiku(line):
    ''' Formats the haiku with newlines '''
    haiku = ""
    for char in line: 
        if char == '/':
            haiku += '/' + '\n'
        else:
            haiku += char
    return haiku

def delete_line(data):
    ''' Deletes the first line from haiku.txt '''
    filehandle = open("haiku.txt", "w")
    filehandle.writelines(data[1:])
    filehandle.close()

def check_haiku_file():
    ''' Check wether haiku.txt exists, is accessible and isn't empty
        If one of the above is not the case it will read new haikus to solve it '''
    path = './haiku.txt'
    if os.path.isfile(path) and os.access(path, os.R_OK):
        filehandle = open("haiku.txt", "r")
        line = filehandle.readline()
        filehandle.close()
        if line == "":
            read_haikus(FILENAME)
    else:
        read_haikus(FILENAME)
                
def get_haiku():
    ''' Read haiku from haiku.txt, format it and delete the haiku from haiku.txt '''
    check_haiku_file()   
    filehandle = open("haiku.txt", "r")
    line = filehandle.readline()
    haiku = format_haiku(line)
    
    data = filehandle.read().splitlines(True) 
    delete_line(data)       
    filehandle.close()
    
    return haiku

