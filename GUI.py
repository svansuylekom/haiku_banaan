#!/usr/bin/python3
#
#http://effbot.org/tkinterbook/frame.htm

import tweepy
import tkinter as tk
from tkinter import ttk
from haiku_maker import *
from twitterbot import tweet

LARGE_FONT = ("Verdana", 16, "bold")

class Twitter_Bot(tk.Tk):
	'''Generates the main window and the buttons in it'''
	def __init__(self, *args, **kwargs):

		tk.Tk.__init__(self, *args, **kwargs)

		#tk.Tk.iconbitmap(self, default="program_icon.ico")
		tk.Tk.wm_title(self, "Haiku Banaan")

		container = tk.Frame(self)
		container.pack(side="top", fill="both", expand = True)
		container.grid_rowconfigure(0, weight=1)
		container.grid_columnconfigure(0, weight=1)

		self.frames = {}
		frame = Window_Buttons(container, self)
		self.frames[Window_Buttons] = frame
		frame.grid(row=0, column=0, sticky="nsew")
		self.show_frame(Window_Buttons)

	def show_frame(self, cont):

		frame = self.frames[cont]
		frame.tkraise()

class Haiku():
    '''Runs the functions from other programs when a button is pressed and
        stores the haiku '''
    def generate(self):
	    '''Runs get_haiku from haiku_maker.py'''
	    self.haiku = get_haiku()
	    print(self.haiku)

    def tweet_haiku(self):
	    '''Runs tweet from twitterbot.py'''
	    try:
	        tweet(self.haiku)
	        print("Tweet succesfully sent")
	    except tweepy.error.TweepError:
	        print("Tweet has already been sent, generate a new one")


class Window_Buttons(tk.Frame):
	'''Generates the buttons'''
	def __init__(self, parent, controller):
		tk.Frame.__init__(self,parent)
		#Back ground label
		label = tk.Label(self, text="Haiku Banaan", font=LARGE_FONT)
		label.pack(pady=15,padx=0) #spacing

		#Generate Haiku button
		button1 = ttk.Button(self, text="Generate Haiku", 
			command=lambda: Haiku.generate(self))
		button1.pack(pady=0,padx=50) #spacing

		#Tweet Haiku Button
		button2 = ttk.Button(self, text="Tweet Haiku",
			command=lambda: Haiku.tweet_haiku(self))
		button2.pack(pady=20,padx=0) #spacing


app = Twitter_Bot()
app.mainloop()
