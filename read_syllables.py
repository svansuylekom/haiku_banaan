#!/usr/bin/python3

import csv
                                                                                                                                                    
def export_dictionairy(literals):
    ''' Write contents of literals to file called literals.csv '''
    literals_csv = csv.writer(open("literals.csv", "w"))
    for key, val in literals.items():
        literals_csv.writerow([key, val])    

def get_syllables(line):
    ''' Reads the word and nr of syllables from the line and saves it in literals'''
    slash_counter = stripe_counter = 0
    word = ""
        
    for char in line:
        # nr of stripes +1 is amount of syllables in one word
        if char == '-':
            stripe_counter += 1
        elif char == '\\':
            slash_counter += 1
        if slash_counter == 1 and char != '\\':
            # The word stands in between the 1st and 2nd slash
            word += char
            if char == ' ':
               # In case of multiple words, syllables +1 becomes syllables + nr of words
               stripe_counter += 1
    stripe_counter += 1
    
    return word, stripe_counter  

def main():
    literals = {}
    filehandle = open("dpw.cd", "r")
    
    for line in filehandle:
        word, nr_syllables = get_syllables(line)
        literals[word] = nr_syllables

    filehandle.close()
    export_dictionairy(literals)

if __name__ == "__main__":
    main()

